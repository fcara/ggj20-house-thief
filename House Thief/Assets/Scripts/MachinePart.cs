﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachinePart : MonoBehaviour
{
    [SerializeField]
    private PartType partType;
    public PartType PartType{get{return partType;}}
    public PlayerController Graber{get;set;}
    public MachineSlot Slot{get;set;}
}
