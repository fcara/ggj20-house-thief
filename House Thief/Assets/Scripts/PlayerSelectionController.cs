using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlayerSelectionController : MonoBehaviour {

    private Dropdown dropdown;
    public int index{get{return dropdown.value;}}
    public Color color;
    public int colorID;

    private void Awake() {
        dropdown = GetComponent<Dropdown>();
    }
    public void OnPlayerSelected(int playerIndex){
        MenuController.Instance.OnPlayerSelected();
    }
}