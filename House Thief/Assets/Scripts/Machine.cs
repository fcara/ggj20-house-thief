﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : MonoBehaviour
{
    public PlayerController Owner {get;set;}
    [SerializeField]
    private List<MachineSlot> machineSlots;
    public List<MachineSlot> MachinesSlots{get{return machineSlots;}}
    public Color color { get; set; }

    [SerializeField]
    private AudioClip autoCompletado;
    
    [SerializeField]
     private AudioSource audioSRC;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMachineSlotCompleted(){
        CheckMachineParts();
    }

    public void CheckMachineParts(){
        if(!MachinesSlots.Exists(machineSlot=>machineSlot.MachinePartSet==null)){
            audioSRC.PlayOneShot(autoCompletado);
            GameManager.Instance.OnMachineCompleted(this);
        }
    }

    public int CalcPartsCompleted(){
        return MachinesSlots.FindAll(machineSlot=>machineSlot.MachinePartSet!=null).Count;
    }

    public float GetLastTimeCompleted(){
        float lastPutTotalTime = 0;
        MachinesSlots.ForEach(machineSlot=>{
            if(machineSlot.LastPutTime>lastPutTotalTime){
                lastPutTotalTime = machineSlot.LastPutTime;
            }
        });
        return lastPutTotalTime;
    }
}
