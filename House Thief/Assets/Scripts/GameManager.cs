﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance{
        get{
            if(instance==null)
                instance = FindObjectOfType<GameManager>();
            return instance;
        }
    }

    public static List<PlayerData> players;
    public static bool modeFast = false;

    [SerializeField]
    private Machine machinePrefab;


    [SerializeField]
    private PlayerController redPlayerPrefab; //id 0
    [SerializeField]
    private PlayerController bluePlayerPrefab; //id 1
    [SerializeField]
    private PlayerController yellowPlayerPrefab; //id 2
    [SerializeField]
    private PlayerController greenPlayerPrefab; //id 3

    [SerializeField]
    private List<MachinePart> partPrefabs;

    [SerializeField]
    private List<Transform> playerPositions;
    [SerializeField]
    private List<Transform> machinePositions;
    [SerializeField]
    private Transform partsSpawnPointsParent;
    private Dictionary<PartType,MachinePart> prefabDictionary;
    [SerializeField]
    private Transform stage;
    private List<Machine> machinesSpawned = new List<Machine>();
    private List<PlayerController> playersSpawned = new List<PlayerController>();

    [SerializeField]
    private Text timeLeftNoTxt;
    [SerializeField]
    private float timeLeft = 60;
    public float TimeLeft {
        get{return timeLeft;} 
        set{
            timeLeft = value;
            if(timeLeft<0)
                timeLeft = 0;
            timeLeftNoTxt.text = Mathf.CeilToInt(timeLeft).ToString();
        }
    }

    [SerializeField]
    private int partMissingPerMachine;

    [SerializeField]
    private GameOverController gameOverController;

    private void Awake() {
        prefabDictionary = new Dictionary<PartType, MachinePart>(partPrefabs.Count);
        foreach(MachinePart part in partPrefabs){
            prefabDictionary.Add(part.PartType,part);
        }
        if(players==null){
            players = new List<PlayerData>(2);
            players.Add(new PlayerData(1,Color.red, 0));
            players.Add(new PlayerData(2,Color.blue, 1));
        }
    }

    private void Start() {
        Debug.Log("joy count: "+Input.GetJoystickNames().Length);
        foreach (string joy in Input.GetJoystickNames()) {
            Debug.Log(joy);
        }
        InitializePlayers();
        InitializeParts();
    }

    private void Update() {
        if(Input.GetKeyUp(KeyCode.Escape))
            SceneManager.LoadScene(0);
            if(TimeLeft>0){
            TimeLeft-=Time.deltaTime;
            if(TimeLeft<=0)
                GameOver(null);
        }
    }

    private void InitializePlayers(){
        PlayerData playerData;
        PlayerController playerPrefab;
        for(int i=0;i<players.Count;i++){
            playerData = players[i];
            switch (playerData.colorID) {
                case 0:
                    playerPrefab = redPlayerPrefab;
                    break;
                case 1:
                    playerPrefab = bluePlayerPrefab;
                    break;
                case 2:
                    playerPrefab = yellowPlayerPrefab;
                    break;
                case 3:
                    playerPrefab = greenPlayerPrefab;
                    break;
                default:
                    playerPrefab = redPlayerPrefab;
                    break;
            }
            PlayerController player = Instantiate<PlayerController>(playerPrefab, playerPositions[i].position,playerPositions[i].rotation, stage);
            Machine machine = Instantiate<Machine>(machinePrefab, machinePositions[i].position,machinePositions[i].rotation, stage);
            machine.Owner = player;
            machine.color = playerData.color;
            player.PlayerNo = i;
            playersSpawned.Add(player);
            machinesSpawned.Add(machine);
        }
    }

    private void InitializeParts(){
        int partMissing = 0;
        Machine machine;
        int machineParts = machinePrefab.MachinesSlots.Count;
        int partsLeftToRemove;
        int spawnPositionIndex;
        List<Transform> spawnPositions = new List<Transform>(partsSpawnPointsParent.GetComponentsInChildren<Transform>());
        Vector3 spawnPosition = Vector3.up*.5f; //TODO
        for(int i=0;i<machinesSpawned.Count;i++){
            machine = machinesSpawned[i];
            partsLeftToRemove = partMissingPerMachine;
            for(int j=0;j<machine.MachinesSlots.Count;j++){
                if(j==partMissing && partsLeftToRemove>0){
                    partMissing = (partMissing + 1)%machineParts;
                    partsLeftToRemove--;
                    continue;
                }else{
                    spawnPositionIndex = Random.Range(0,spawnPositions.Count);
                    spawnPosition = spawnPositions[spawnPositionIndex].position;
                    spawnPositions.RemoveAt(spawnPositionIndex);
                    MachinePart machinePart = Instantiate<MachinePart>(prefabDictionary[machine.MachinesSlots[j].PartType],spawnPosition,Quaternion.identity,stage);
                }
                //This below is to cover the loop, but it shouldn't be this way....
                if(partsLeftToRemove>0 && j+1>=machine.MachinesSlots.Count){
                    while(partsLeftToRemove>0){
                        partMissing = (partMissing + 1)%machineParts;
                        partsLeftToRemove--;
                    }
                }
            }
        }
    }

    private void GameOver(Machine machine){
        timeLeft = 0;
        if(machine==null){
            int mostPartsCompleted = 0;
            float lastPartCompletedTime = 0;
            int partsCompleted;
            machinesSpawned.ForEach (machinesSpawned=>{
                partsCompleted = machinesSpawned.CalcPartsCompleted();
                if(partsCompleted>mostPartsCompleted ||
                partsCompleted==mostPartsCompleted && machinesSpawned.GetLastTimeCompleted()>lastPartCompletedTime){
                    mostPartsCompleted = partsCompleted;
                    lastPartCompletedTime = machinesSpawned.GetLastTimeCompleted();
                    machine = machinesSpawned;
                }
            });
        }
        gameOverController.ShowGameOver(machine);
    }

    public void OnMachineCompleted(Machine machine){
        GameOver(machine);
    }

    public void Replay(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
