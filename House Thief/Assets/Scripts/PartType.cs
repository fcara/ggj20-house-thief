public enum PartType{
    WHEEL,
    ENGINE,
    EXHAUST_PIPE,
    LIGHT,
    DOOR
}