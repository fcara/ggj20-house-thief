﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    private static MenuController instance;
    public static MenuController Instance{
        get{
            if(instance == null)
                instance = FindObjectOfType<MenuController>();
            return instance;
            }
    }
    [SerializeField]
    private List<PlayerSelectionController> playerSelectionControllers;
    private Dictionary<int,PlayerData> players = new Dictionary<int, PlayerData>();
    private int playerCount = 0;
    public int PlayerCount{get{return playerCount;} set{
        playerCount = value;
        playFastBtn.interactable = playBtn.interactable = playerCount>=2;
    }}
    [SerializeField]
    private Button playBtn;
    [SerializeField]
    private Button playFastBtn;

    private void Awake() {
        playFastBtn.interactable = playBtn.interactable = false;
    }
    private void Update() {
        if(Input.GetKeyUp(KeyCode.Escape))
            Quit();
    }

    public void Quit(){
        Application.Quit();
    }

    public void SetPlayer(int player, Color color, int colorID) {
        if(players.ContainsKey(player)){
            players[player] = new PlayerData(player,color, colorID);
        }else{
            players.Add(player, new PlayerData(player,color, colorID));
        }
    }


    public void Play(){
        SetPlayersInGameManager();
        GameManager.modeFast = false;
        SceneManager.LoadScene(1);
    }
    public void PlayFast(){
        SetPlayersInGameManager();
        GameManager.modeFast = true;
        SceneManager.LoadScene(1);
    }

    private void SetPlayersInGameManager(){
        List<PlayerData> playersList = new List<PlayerData>(players.Count);
        foreach(KeyValuePair<int,PlayerData> pair in players){
            playersList.Add(pair.Value);
        }
        GameManager.players = playersList;
    }

    public void OnPlayerSelected(){
        players.Clear();
        foreach (PlayerSelectionController playerSelectionController in playerSelectionControllers)
        {
            if(playerSelectionController.index==0)
                continue;
            else{
                if(players.ContainsKey(playerSelectionController.index))
                {
                    PlayerCount = 0;
                    break;
                }else{
                    players.Add(playerSelectionController.index,new PlayerData(playerSelectionController.index,playerSelectionController.color, playerSelectionController.colorID));
                }
            }
        }
        PlayerCount = players.Count;
    }
}
