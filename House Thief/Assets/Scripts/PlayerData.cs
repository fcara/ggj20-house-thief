using System;
using UnityEngine;

[Serializable]
public class PlayerData{
    public int id;
    public Color color;
    public int colorID;

    public PlayerData(int id, Color color, int colorID) {
        this.id = id;
        this.color = color;
        this.colorID = colorID;
    }
}