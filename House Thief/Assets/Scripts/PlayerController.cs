﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private PlayerHands hands;
    [SerializeField]
    private float hitForce = 10;
    [SerializeField]
    private float hitStunDuration = 1;

    public int PlayerNo {get; set;}

    [SerializeField]
    private Vector2 speed = Vector2.one;

    private Vector2 input;

    private MachinePart holdedMachinePart;
    public MachinePart HoldedMachinePart{get{return holdedMachinePart;}}


    private Rigidbody rb;
    private string horizontalInput;
    private string verticalInput;

    private float stunnedTimeLeft = 0;

    [SerializeField]
    private AudioClip audioGolpe;

    [SerializeField]
    private AudioClip audioRobarParte;
    
    [SerializeField]
    private AudioClip audioReparacion;

    [SerializeField]
     private AudioSource audioSRC;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        horizontalInput = PlayerNo==0?"Horizontal" : ("Horizontal_"+PlayerNo);
        verticalInput = PlayerNo==0?"Vertical" : ("Vertical_"+PlayerNo);
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw(horizontalInput);
        horizontal = Mathf.RoundToInt(horizontal);
        float vertical = Input.GetAxisRaw(verticalInput);
        vertical = Mathf.RoundToInt(vertical);
        input = new Vector3(horizontal,vertical);
    }

    private void FixedUpdate() {
        if(GameManager.Instance.TimeLeft<=0)
            return;
        if(stunnedTimeLeft<=0){
            rb.velocity = new Vector3(input.x*speed.x,0,input.y*speed.y);
        }else{
            stunnedTimeLeft -= Time.fixedDeltaTime;
            if(stunnedTimeLeft<0)
                stunnedTimeLeft = 0;
        }
        float directionAngle = 0;
        if(input.x>0&&input.y==0){
            directionAngle = 0;
        }else if(input.x>0&&input.y<0){
            directionAngle = 45;
        }else if(input.x>0&&input.y>0){
            directionAngle = -45;
        }else if(input.x<0&&input.y==0){
            directionAngle = 180;
        }else if(input.x<0&&input.y<0){
            directionAngle = 135;
        }else if(input.x<0&&input.y>0){
            directionAngle = 225;
        }else if(input.x==0&&input.y<0){
            directionAngle = 90;
        }else if(input.x==0&&input.y>0){
            directionAngle = -90;
        }else{
            directionAngle = transform.eulerAngles.y;
        }
        transform.eulerAngles = new Vector3(transform.eulerAngles.x,directionAngle,transform.eulerAngles.z);
        //rb.AddForce(input*speed,ForceMode.Impulse);
    }

    public void GrabMachinePart(MachinePart machinePart){
        if(machinePart.Slot!=null)
            machinePart.Slot.MachinePartSet=null;
        machinePart.Slot = null;
        machinePart.Graber = this;
        if(holdedMachinePart!=null){
            DropHoldedMachinePart();
        }
        holdedMachinePart = machinePart;
    }

    public void Hit(Vector3 hitOrigin){
        Vector3 dir = transform.position-hitOrigin;
        dir.y = 0;
        dir.Normalize();
        rb.AddForce(dir*hitForce,ForceMode.Force);
        
        audioSRC.PlayOneShot(audioGolpe);
        
        DropHoldedMachinePart();
        stunnedTimeLeft = hitStunDuration;
    }

    public void PutPart(MachineSlot slot){

        slot.MachinePartSet = HoldedMachinePart;
        HoldedMachinePart.Graber = null;
        holdedMachinePart = null;

        audioSRC.PlayOneShot(audioReparacion);
    }

    public void DropHoldedMachinePart(){
        if(holdedMachinePart!=null){
            holdedMachinePart.Slot = null;
            holdedMachinePart.Graber = null;
            holdedMachinePart.transform.SetParent(null);
        }
    }
    
}
