using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineSlot:MonoBehaviour{
    [SerializeField]
    private Machine owner;
    public Machine Owner {get{return owner;}}
    [SerializeField]
    private PartType partType;
    public PartType PartType{get{return partType;}}
    private MachinePart machinePartSet = null;
    [SerializeField]
    private Transform slotTransform;

    private float lastPutTime;
    public float LastPutTime{get{return lastPutTime;}}
    public MachinePart MachinePartSet{
        get{return machinePartSet;}
        set{
            if(machinePartSet!=null && value==null)
                machinePartSet.transform.SetParent(null);
            machinePartSet=value;
            if(machinePartSet!=null){
                machinePartSet.Slot = this;
                machinePartSet.transform.SetParent(slotTransform);
                machinePartSet.transform.localPosition = Vector3.zero;
                machinePartSet.transform.localRotation = Quaternion.identity;
                machinePartSet.transform.localScale = Vector3.one;
                lastPutTime = Time.time;
                owner.OnMachineSlotCompleted();
            }
        }
    }
}