using UnityEngine;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour {
    [SerializeField]
    private GameObject main;
    [SerializeField]
    private Transform machineHolder;
    [SerializeField]
    private GameObject noWinner;
    public void ShowGameOver(Machine winnerMachine){
        main.SetActive(true);
        if(winnerMachine!=null){
            Machine winnerMachineInstance = Instantiate<Machine>(winnerMachine);
            winnerMachineInstance.transform.SetParent(machineHolder);
            machineHolder.GetComponentInChildren<Image>().color = winnerMachine.color;
        }else{
            noWinner.SetActive(true);
            machineHolder.gameObject.SetActive(false);
        }
    }
}