using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHands: MonoBehaviour{

    [SerializeField]
    private PlayerController player;
    private PlayerController playerInContact = null;
    private MachinePart machinePartInContact = null;

    private List<MachineSlot> machineSlotsInContact = new List<MachineSlot>();

    private string fire1Input;

    private void Start() {
        fire1Input = "Fire1_" + player.PlayerNo;
    }

    public bool CanDepositPart{
            //empty slot, same type and player holding part
        get{ 
            MachineSlot machineSlotInContact = player.HoldedMachinePart!=null?
        machineSlotsInContact.Find(slot=>slot.PartType==player.HoldedMachinePart.PartType) : null;
            return machineSlotInContact!=null && machineSlotInContact.MachinePartSet==null;}
    }
    public bool CanStealPart{

            //filled slot from an enemy machine and player not holding anything
        get{ return player.HoldedMachinePart==null && machineSlotsInContact.Exists(slot=>slot.MachinePartSet!=null && slot.Owner.Owner!=player);}
    }

    private void Update() {

        if(GameManager.Instance.TimeLeft<=0)
            return;
            
        if(Input.GetButtonUp(fire1Input)){
            if(playerInContact!=null){
                playerInContact.Hit(transform.position);
            } else if(machineSlotsInContact.Count>0 && 
            (CanDepositPart || CanStealPart)){
                MachineSlot machineSlotInContact;
                if(CanStealPart){
                    machineSlotInContact = machineSlotsInContact.Find(slot=>slot.MachinePartSet!=null);
                    if(machineSlotsInContact==null)
                    {
                        Debug.LogError("WTF");
                        return;
                    }
                    GrabMachinePart(machineSlotInContact.MachinePartSet);
                }else if(CanDepositPart){
                    machineSlotInContact = machineSlotsInContact.Find(slot=>slot.PartType==player.HoldedMachinePart.PartType);
                    player.PutPart(machineSlotInContact);
                }
            } else if(machinePartInContact!=null && machinePartInContact.Slot==null){
                GrabMachinePart(machinePartInContact);
            }
        }
    }

    private void GrabMachinePart(MachinePart machinePart){
        player.GrabMachinePart(machinePart);
        machinePart.transform.SetParent( transform );
        machinePart.transform.localPosition = Vector3.zero;
        if(machinePartInContact==machinePart){
            machinePartInContact = null;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.tag=="MachinePart"){
            MachinePart tempMP = other.GetComponent<MachinePart>();
            if(tempMP.Graber!=player){
                machinePartInContact = tempMP;
            }
        }else if(other.tag=="MachineSlot" &&
         !machineSlotsInContact.Contains(other.GetComponent<MachineSlot>()) ){
            machineSlotsInContact.Add(other.GetComponent<MachineSlot>());
        } else if(other.tag=="Player" && other.gameObject!=player.gameObject){
            playerInContact = other.GetComponent<PlayerController>();
        }
    }
    private void OnTriggerExit(Collider other) {
        if(machinePartInContact!=null && other.tag=="MachinePart" && other.gameObject==machinePartInContact.gameObject){
            machinePartInContact = null;
        } else if(machineSlotsInContact.Count>0 && other.tag == "MachineSlot" && machineSlotsInContact.Exists(slot=>slot.gameObject==other.gameObject)){
            machineSlotsInContact.RemoveAll(slot=>slot.gameObject==other.gameObject);
        } else if(playerInContact!=null && other.tag == "Player" && other.gameObject==playerInContact.gameObject){
            playerInContact = null;
        }
    }
}